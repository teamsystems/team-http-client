# TEAM HTTP Client
This is a PHP HTTP client & micro-framework for building RESTful web service clients.
Provides the power of Curl, with a simple to use and lightweight interface.

Fetch a page:
```php
$x = TEAM\HttpClient\Request::get('http://birchwood.ca')
 	->addHeader('X-Requested-By', 'AppName')
 	->send();

echo $x;
```

Retrieve and parse JSON or XML with authentication:
```php
$x = TEAM\HttpClient\Request::get('http://birchwood.ca/makes.json')
 	->setAuth('mfrank', 'twistAndShout')
 	->send();

// If the Content-Type is set properly:
$aJson = $x->parsed();
// otherwise...
$aJson = $x->parseAs('json');
```

Post a file:
```php
$x = TEAM\HttpClient\Request::post('http://birchwood.ca', array(
 	'file' => new CurlFile('/tmp/resume.doc')
))->send();
```

Post XML body:
```php
$x = TEAM\HttpClient\Request::post('http://birchwood.ca',
 	'<xml><name>Mike</name></xml>',
 	'application/xml')->send();
```

Fetch meta data from the response:
```php
// Get a header
echo $x->getHeader('content-type');

// Read a cookie value
echo $x->getCookie('PHPSESSID');

// Get the status code
echo $x->iStatus;
```

## Install with Composer
To install with [Composer](https://getcomposer.org/), simply require the latest version of this package.
```
composer require team/httpclient
```