<?php

namespace TEAM\HttpClient;

/**
 * Response class for results of HTTP requests
 *
 * @package		team-http-client
 * @author		Mike Frank <mike.frank@teamsystems.ca>
 */
class Response
{
	/**
	 * Url of response
	 * @var string
	 */
	public $sUrl;

	/**
	 * Meta information on response
	 * @var array
	 */
	public $aMeta;

	/**
	 * HTTP status code
	 * @var int
	 */
	public $iStatus;

	/**
	 * Content type of response body
	 * @var string
	 */
	public $sContentType;

	/**
	 * Character encoding of response
	 * @var string
	 */
	public $sCharset;

    /**
     * Response body size
     * @var int
     */
    public $iSize;

	/**
	 * Raw headers
	 * @var string
	 */
	public $sHeaders;

	/**
	 * Raw body
	 * @var string
	 */
	public $sBody;

	/**
	 * Parsed headers
	 * @var array
	 */
	public $aHeaders = array();


	/**
	 * Cookie Jar
	 * @var CookieJar
	 */
	public $oCookieJar;

	/**
	 * Parsed body result
	 *
	 * @var mixed
	 */
	protected $_mParsed;

	/**
	 * Response constructor.
	 * @param $sUrl
	 * @param $sHeaders
	 * @param $sBody
	 * @param array $aMeta
	 */
	public function __construct($sUrl, $sHeaders, $sBody, array $aMeta = array())
	{
		$this->sUrl = $sUrl;
		$this->aMeta = $aMeta;
		$this->sHeaders  = $sHeaders;
		$this->sBody = $sBody;
		$this->iStatus = $aMeta['http_code'];
		$this->iSize = $aMeta['size_download'];

        if ( isset($aMeta['content_type']) ) {

            $aTypeMatches = array();
            preg_match('/^(.*?)(; *charset=(.+))?$/i', $aMeta['content_type'], $aTypeMatches);

            if ( isset($aTypeMatches[1]) ) {
                $this->sContentType = strtolower($aTypeMatches[1]);
            }

            if ( isset($aTypeMatches[3]) ) {
                $this->sCharset = strtolower($aTypeMatches[3]);
            }

        }

		$aHeaders = array_map('trim', explode("\n", $this->sHeaders));

		$this->oCookieJar = new CookieJar($this->sUrl);

		foreach($aHeaders as $sHeader) {
			$sHeader = trim($sHeader);

			if ( $sHeader and ! preg_match('#^HTTP/#i', $sHeader) ) {
				list($sName, $sValue) = explode(':', $sHeader, 2);

				$sName = trim(strtolower($sName));
				$sValue = trim($sValue);

				if ( $sName == 'set-cookie' ) {
					$this->oCookieJar->add($sValue);
				}

				if ( array_key_exists($sName, $this->aHeaders) ) {
					if ( ! is_array($this->aHeaders[$sName]) ) {
						$this->aHeaders[$sName] = array($this->aHeaders[$sName]);
					}

					$this->aHeaders[$sName][] = $sValue;
				}
				else {
					$this->aHeaders[$sName] = $sValue;
				}
			}
		}
	}

	/**
	 * Retrieve a header
	 *
	 * @param string $sName
	 * @return array|string
	 */
	public function getHeader($sName)
	{
		$sReturn = '';

		$sName = strtolower($sName);

		if ( array_key_exists($sName, $this->aHeaders) ) {
			$sReturn = $this->aHeaders[$sName];
		}

		return $sReturn;
	}

	public function getCookie($sName)
	{
		$sReturn = '';

		foreach($this->oCookieJar->get($this->sUrl) as $aCookie) {
			if ( $sName == $aCookie['value']['key'] ) {
				$sReturn = $aCookie['value']['value'];
				break;
			}
		}

		return $sReturn;
	}

	/**
	 * Fetch the parsed result of the body
	 *
	 * @return mixed
	 */
	public function parsed()
	{
		return $this->parseAs($this->sContentType);
	}

	/**
	 * Parse the body as a certain format
	 *
	 * @param string $sMime
	 * @return mixed
	 */
	public function parseAs($sMime)
	{
		if ( $this->_mParsed === NULL ) {
			switch ( $sMime )
			{
				case 'application/json':
				case 'json':
					$this->_mParsed = json_decode($this->sBody);
					break;

				case 'application/xml':
				case 'text/xml':
				case 'xml':
					$this->_mParsed = simplexml_load_string($this->sBody);
					break;

				case 'php':
					$this->_mParsed = unserialize($this->sBody);
					break;

				case 'text/html':
				case 'html':
					$oDom = new \DOMDocument();
					libxml_use_internal_errors(FALSE);
					$oDom->loadHTML($this->sBody);

					$this->_mParsed = $oDom;
					break;

				default:
					$this->_mParsed = $this->sBody;
			}
		}

		return $this->_mParsed;
	}

	/**
	 * Apply a callback to the body
	 *
	 * @param callable $funcCallback
	 * @return mixed
	 */
	public function parseWith($funcCallback)
	{
		return $funcCallback($this->sBody);
	}

	public function __toString()
	{
		return $this->sBody;
	}
}