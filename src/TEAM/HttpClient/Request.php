<?php

namespace TEAM\HttpClient;

/**
 * This is a PHP HTTP client & micro-framework for building RESTful web service clients.
 *
 * Provides the power of Curl, with a simple to use and lightweight interface.
 *
 * Fetch a page:
 * <code>
 * $x = TEAM\HttpClient\Request::get('http://birchwood.ca')
 *	->addHeader('X-Requested-By', 'AppName')
 *	->send();
 *
 * echo $x;
 * </code>
 *
 * Retrieve and parse JSON or XML with authentication:
 * <code>
 * $x = TEAM\HttpClient\Request::get('http://birchwood.ca/makes.json')
 *	->setAuth('mfrank', 'twistAndShout')
 *	->send();
 *
 * // If the Content-Type is set properly:
 * $aJson = $x->parsed();
 * // otherwise...
 * $aJson = $x->parseAs('json');
 * </code>
 *
 * Post a file:
 * <code>
 * $x = TEAM\HttpClient\Request::post('http://birchwood.ca', array(
 *	'file' => new CurlFile('/tmp/resume.doc')
 * ))->send();
 * </code>
 *
 * Post XML body:
 * <code>
 * $x = TEAM\HttpClient\Request::post('http://birchwood.ca',
 *	'<xml><name>Mike</name></xml>',
 *	'application/xml')->send();
 * </code>
 *
 * Fetch meta data from the response:
 * <code>
 * // Get a header
 * echo $x->getHeader('content-type');
 *
 * // Read a cookie value
 * echo $x->getCookie('PHPSESSID');
 *
 * // Get the status code
 * echo $x->iStatus;
 * </code>
 *
 * @package		team-http-client
 * @author		Mike Frank <mike.frank@teamsystems.ca>
 */
class Request
{
	const METHOD_GET = 1;
	const METHOD_POST = 2;
	const METHOD_HEAD = 3;
	const METHOD_DELETE = 4;
	const METHOD_PUT = 5;

	const AUTH_BASIC = 1;
	const AUTH_DIGEST = 2;

	const USER_AGENT = 'TEAM Framework/HTTP Client';

	/**
	 * Request URL
	 *
	 * @var string
	 */
	protected $_sUrl;

	/**
	 * Transport method
	 * @var int
	 */
	protected $_iMethod;

	/**
	 * Post body
	 * @var mixed
	 */
	protected $_mBody;

	/**
	 * Post data type
	 * @var string
	 */
	protected $_sMime;

	/**
	 * Headers to send
	 *
	 * @var array
	 */
	protected $_aHeaders = array();

	/**
	 * Custom user agent
	 *
	 * @var string
	 */
	protected $_sUserAgent;

	/**
	 * Custom CURL options
	 * @var array
	 */
	protected $_aCurlOpts = array();

	/**
	 * Authentication username
	 * @var string
	 */
	protected $_sUsername;

	/**
	 * Authentication password
	 * @var string
	 */
	protected $_sPassword;

	/**
	 * Authentication method
	 *
	 * @var int
	 */
	protected $_iAuthMethod;

	/**
	 * SSL certificate file path
	 * @var string
	 */
	protected $_sSslCert;

	/**
	 * SSL key file path
	 * @var string
	 */
	protected $_sSslKey;

	/**
	 * SSL passphrase
	 * @var string
	 */
	protected $_sSslPassphrase;

	/**
	 * SSL enconding method
	 * @var string
	 */
	protected $_sSslEncoding;

	/**
	 * Use strict ssl checking
	 * @var bool
	 */
	protected $_bStrictSsl;

	/**
	 * Follow Location: headers
	 * @var bool
	 */
	protected $_bFollow = TRUE;

	/**
	 * Amount of seconds before timing out
	 * @var int
	 */
	protected $_iTimeout = NULL;

	/**
	 * Cookie Jar
	 *
	 * @var CookieJar
	 */
	protected $_oCookieJar;

    /**
     * File resource to write to
     * @var resource
     */
    protected $_rFp;

	/**
	 *
	 * @param string $sUrl
	 * @param int $iMethod
	 */
	public function __construct($sUrl = '', $iMethod = self::METHOD_GET)
	{
		assert( is_string($sUrl) and is_int($iMethod) );

		$this->_sUrl = $sUrl;
		$this->_iMethod = $iMethod;
	}

	public function setUrl($sUrl)
	{
		$this->_sUrl = $sUrl;

		return $this;
	}

	public function setMethod($iMethod)
	{
		$this->_iMethod = $iMethod;

		return $this;
	}

	/**
	 * Add a custom header to the request
	 *
	 * @param string|array $mName
	 * @param string $sValue
	 * @return Request
	 */
	public function addHeader($mName, $sValue = NULL)
	{
		assert(is_string($mName) or is_array($mName));

		if ( is_array($mName) and $sValue === NULL ) {
			foreach($mName as $sName => $sValue) {
				$this->addHeader($sName, $sValue);
			}
		}
		else {
			$this->_aHeaders[] = $mName . ': ' . $sValue;
		}

		return $this;
	}

	/**
	 * Set the post body contents
	 *
	 * @param mixed $mBody
	 * @param string $sMime
	 * @return Request
	 */
	public function body($mBody, $sMime = NULL)
	{
		$this->_mBody = $mBody;

		if ( $sMime ) {
			$this->_sMime = $sMime;
		}

		return $this;
	}

	/**
	 * Set the authentication method
	 *
	 * @param string $sUsername
	 * @param string $sPassword
	 * @param int $iAuthMethod
	 * @return Request
	 */
	public function setAuth($sUsername, $sPassword, $iAuthMethod = self::AUTH_BASIC)
	{
		assert(is_int($iAuthMethod));

		$this->_sUsername = $sUsername;
		$this->_sPassword = $sPassword;
		$this->_iAuthMethod = $iAuthMethod;

		return $this;
	}

	/**
	 * Set the user agent
	 *
	 * @param string $sAgent
	 * @return Request
	 */
	public function setUserAgent($sAgent)
	{
		assert(is_string($sAgent));

		$this->_sUserAgent = $sAgent;

		return $this;
	}

	/**
	 * Set custom options to pass to curl
	 *
	 * @param int $iOpt
	 * @param mixed $mValue
	 * @return Request
	 */
	public function setCurlOpt($iOpt, $mValue)
	{
		$this->_aCurlOpts[$iOpt] = $mValue;

		return $this;
	}

	/**
	 * Use strict SSL
	 *
	 * @param bool $bStrictSsl
	 * @return Request
	 */
	public function useStrictSsl($bStrictSsl)
	{
		assert(is_bool($bStrictSsl));

		$this->_bStrictSsl = $bStrictSsl;

		return $this;
	}

	/**
	 * Follow "Location:" headers
	 *
	 * @param bool $bFollow
	 * @return self
	 */
	public function followLocation($bFollow)
	{
		$this->_bFollow = $bFollow;

		return $this;
	}

	/**
	 * Set the certificate info
	 *
	 * @param string $sCert
	 * @param string $sKey
	 * @param string $sPassphrase
	 * @param string $sEncoding
	 * @return Request
	 */
	public function clientSideCert($sCert, $sKey, $sPassphrase = NULL, $sEncoding = 'PEM')
	{
		$this->_sSslCert = $sCert;
		$this->_sSslKey = $sKey;
		$this->_sSslPassphrase = $sPassphrase;
		$this->_sSslEncoding = $sEncoding;

		return $this;
	}

	/**
	 * Set the cookie jar to be used by this request
	 *
	 * @param CookieJar $oJar
	 * @return Request
	 */
	public function setCookieJar(CookieJar $oJar)
	{
		$this->_oCookieJar = $oJar;

		return $this;
	}

	/**
	 * Set the timeout of the connection
	 *
	 * @param int $iTimeout Timeout in seconds
	 * @return self
	 */
	public function setTimeout($iTimeout)
	{
		assert(is_int($iTimeout) or is_null($iTimeout));

		$this->_iTimeout = $iTimeout;

        return $this;
	}

    /**
     * Output the response body a file stream
     *
     * @param resource $rFp File stream
	 * @return self
     */
    public function file($rFp)
    {
        assert(is_resource($rFp) or is_null($rFp));

        $this->_rFp = $rFp;

        return $this;
    }

	/**
	 * Send the request
	 *
	 * @throws Exception
	 * @return Response
	 */
	public function send()
	{
		$sUrl = $this->_sUrl;
		if ( $this->_iMethod == self::METHOD_GET and is_array($this->_mBody) and count($this->_mBody) > 0 ) {
			$sUrl = $this->_sUrl . '?' . http_build_query($this->_mBody);
		}


		$rCurl = curl_init();
		curl_setopt($rCurl, CURLOPT_HEADER, TRUE);
		curl_setopt($rCurl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($rCurl, CURLINFO_HEADER_OUT, TRUE);




		$aHeaders = $this->_aHeaders;

		if ( $this->_sMime ) {
			$aHeaders[] = 'Content-Type: ' . $this->_sMime;
		}

		switch ($this->_iMethod) {
			case self::METHOD_POST:
				curl_setopt($rCurl, CURLOPT_CUSTOMREQUEST, 'POST');
				break;

			case self::METHOD_HEAD:
                curl_setopt($rCurl, CURLOPT_NOBODY, TRUE);
				break;

			case self::METHOD_PUT:
				curl_setopt($rCurl, CURLOPT_CUSTOMREQUEST, 'PUT');
				break;

			case self::METHOD_DELETE:
				curl_setopt($rCurl, CURLOPT_CUSTOMREQUEST, 'DELETE');
				break;

			default:

				break;
		}

		if ( $this->_sMime == 'application/json' and is_array($this->_mBody) ) {
			$this->_mBody = json_encode($this->_mBody);
		}

		if ( $this->_iMethod != self::METHOD_GET and $this->_mBody ) {
			if ( is_array($this->_mBody) ) {

                if ( defined('CURLOPT_SAFE_UPLOAD') ) {
                    // Force safe upload for files, no exceptions
                    curl_setopt($rCurl, CURLOPT_SAFE_UPLOAD, TRUE);
                }

				// We need to loop though body data and look for any includes of files and account for it

				/*
				 * Notice we're not looking into nested values? It's not possible to pass files in a nested type. This is a limitation of PHP and Curl
				 *  @link http://stackoverflow.com/questions/3453353/how-to-upload-files-multipart-form-data-with-multidimensional-postfields-using
				 */
				$bIncludesFiles = FALSE;
				foreach(array_values($this->_mBody) as $mValue) {
					if ( gettype($mValue) == 'object' and get_class($mValue) == 'CURLFile' ) {
						$bIncludesFiles = TRUE;
						break;
					}
				}

				if ( $bIncludesFiles === TRUE ) {
					// If we're including files, we want to pass the original payload body to curl so it can format it properly
					// @link http://php.net/manual/en/function.curl-setopt.php
					// This will make the content-type multipart/form-data
					curl_setopt($rCurl, CURLOPT_POSTFIELDS, $this->_mBody);
				}
				else {
					curl_setopt($rCurl, CURLOPT_POSTFIELDS, http_build_query($this->_mBody));
				}
			}
			else {
				curl_setopt($rCurl, CURLOPT_POSTFIELDS, $this->_mBody);
			}

			$aHeaders[] = 'Expect:';
		}

		// Handle the cookie jar
		$sCookieHeader = NULL;
		if ( $this->_oCookieJar ) {
			$sCookieHeader = $this->_oCookieJar->buildHeader($sUrl);

			if ( $sCookieHeader ) {
				curl_setopt($rCurl, CURLOPT_COOKIE, $sCookieHeader);
			}
		}

		// Custom headers
		if ( count($aHeaders) > 0 ) {
			curl_setopt($rCurl, CURLOPT_HTTPHEADER, $aHeaders);
		}

		// Authentication
		if ( $this->_sUsername ) {
			curl_setopt($rCurl, CURLOPT_HTTPAUTH, ( $this->_iAuthMethod == self::AUTH_DIGEST ) ? CURLAUTH_DIGEST : CURLAUTH_BASIC );
			curl_setopt($rCurl, CURLOPT_USERPWD, $this->_sUsername . ':' . $this->_sPassword);
		}

		// User agent
		if ( $this->_sUserAgent ) {
			curl_setopt($rCurl, CURLOPT_USERAGENT, $this->_sUserAgent);
		}
		else {
			curl_setopt($rCurl, CURLOPT_USERAGENT, self::USER_AGENT);
		}

		// Certificates
		if ( $this->_sSslCert and $this->_sSslKey ) {
			curl_setopt($rCurl, CURLOPT_SSLCERTTYPE, $this->_sSslEncoding);
			curl_setopt($rCurl, CURLOPT_SSLKEYTYPE, $this->_sSslEncoding);
			curl_setopt($rCurl, CURLOPT_SSLCERT, $this->_sSslCert);
			curl_setopt($rCurl, CURLOPT_SSLKEY, $this->_sSslKey);
			curl_setopt($rCurl, CURLOPT_SSLKEYPASSWD, $this->_sSslPassphrase);
		}

		// Strict SSL
		if ( $this->_bStrictSsl !== NULL ) {
			curl_setopt($rCurl, CURLOPT_SSL_VERIFYPEER, $this->_bStrictSsl);

			// https://bugs.php.net/bug.php?id=63363
			curl_setopt($rCurl, CURLOPT_SSL_VERIFYHOST, ( $this->_bStrictSsl ) ? 2 : 0);
		}


		// Follow location
		$bInternalFollow = FALSE;
		if ( $this->_bFollow !== NULL ) {
			if ( ini_get('open_basedir') or ini_get('safe_mode') ) {
				if ( $this->_bFollow === TRUE ) {
					// open_basedir or safe_mode is in effect, we cant use Curl's follow location, need to use our internal "follower"
					$bInternalFollow = TRUE;
				}
			}
			else {
				curl_setopt($rCurl, CURLOPT_FOLLOWLOCATION, $this->_bFollow);
			}
		}


		// Timeout
		if ( $this->_iTimeout !== NULL ) {
			curl_setopt($rCurl, CURLOPT_TIMEOUT, $this->_iTimeout);
		}

		// Curl custom settings
		foreach ($this->_aCurlOpts as $iOpt => $mValue) {
			curl_setopt($rCurl, $iOpt, $mValue);
		}


		$iMaxRedirs = 5;
		if ( in_array(CURLOPT_MAXREDIRS, $this->_aCurlOpts) ) {
			// Use CURL opt if set
			$iMaxRedirs = $this->_aCurlOpts[CURLOPT_MAXREDIRS];
		}

		$iRedirs = 0;
		do {
            curl_setopt($rCurl, CURLOPT_FILE, $rStream = fopen('php://temp', 'w'));
			curl_setopt($rCurl, CURLOPT_URL, $sUrl);

			$sUrl = NULL;

            $mResponse = curl_exec($rCurl);

			if ( $mResponse === FALSE ) {
				throw new Exception(curl_error($rCurl));
			}

			$aMeta = curl_getinfo($rCurl);

            rewind($rStream);

            $sBody = NULL;
            $sHeaders = fread($rStream, $aMeta['header_size']);

            // Do we output to a file?
            if ( $this->_rFp) {
                // Copy the remainder of the stream to the file, do not rewind first
                stream_copy_to_stream($rStream, $this->_rFp);
            }
            else {
                $sBody = stream_get_contents($rStream);
            }

            fclose($rStream);

            $aHeadersExploded = explode("\r\n\r\n", trim($sHeaders));
            $sHeaders = array_pop($aHeadersExploded);

			$oResponse = new Response($aMeta['url'], $sHeaders, $sBody, $aMeta);

			if ( $sCookieHeader ) {
				// Add cookies from this request to the response object
				$oResponse->oCookieJar->add($sCookieHeader);
			}

			// Do we need to follow-location?
			if ( in_array($aMeta['http_code'], array(301, 302)) and $bInternalFollow === TRUE ) {
				if ( array_key_exists('location', $oResponse->aHeaders) ) {
					$sUrl = $oResponse->aHeaders['location'];
				}
				elseif ( array_key_exists('uri', $oResponse->aHeaders) ) {
					// Non standard but does exists
					$sUrl = $oResponse->aHeaders['uri'];
				}

				if ( $sUrl ) {
					++$iRedirs;

					$sUrl = static::relative2Absolute($sUrl, $this->_sUrl);
				}
			}

		}
		while ( $sUrl !== NULL and $iRedirs < $iMaxRedirs );


		curl_close($rCurl);

		return $oResponse;
	}

	/**
	 * Perform a GET request
	 *
	 * @param string $sUrl
	 * @param string|array $mParams
	 * @return Request
	 */
	public static function get($sUrl, $mParams = NULL)
	{
		return self::_factory($sUrl, self::METHOD_GET, $mParams);
	}

	/**
	 * Perform a POST request
	 *
	 * @param string $sUrl
	 * @param string|array $mPostData
	 * @param string $sMime
	 * @return Request
	 */
	public static function post($sUrl, $mPostData = NULL, $sMime = NULL)
	{
		return self::_factory($sUrl, self::METHOD_POST, $mPostData, $sMime);
	}

	/**
	 * Perform a HEAD request
	 *
	 * @param string $sUrl
	 * @param string|array $mParams
	 * @return Request
	 */
	public static function head($sUrl, $mParams = NULL)
	{
		return self::_factory($sUrl, self::METHOD_HEAD, $mParams);
	}

	/**
	 * Perform a DELETE request
	 *
	 * @param string $sUrl
	 * @param string|array $mBody
	 * @param string $sMime
	 * @return Request
	 */
	public static function delete($sUrl, $mBody = NULL, $sMime = NULL)
	{
		return self::_factory($sUrl, self::METHOD_DELETE, $mBody, $sMime);
	}

	/**
	 * Perform a PUT request
	 *
	 * @param string $sUrl
	 * @param string|array $mBody
	 * @param string $sMime
	 * @return Request
	 */
	public static function put($sUrl, $mBody = NULL, $sMime = NULL)
	{
		return self::_factory($sUrl, self::METHOD_PUT, $mBody, $sMime);
	}

	protected static function _factory($sUrl, $iMethod, $mBody = NULL, $sMime = NULL)
	{
		$oRequest = new Request($sUrl, $iMethod);

		if ( $mBody ) {
			$oRequest->body($mBody, $sMime);
		}

		return $oRequest;
	}

	/**
	 * Convert a relative url to an absolute url
	 *
	 * @link http://nashruddin.com/PHP_Script_for_Converting_Relative_to_Absolute_URL
	 * @param string $sRelUrl
	 * @param string $sBaseUrl
	 * @return string
	 */
	public static function relative2Absolute($sRelUrl, $sBaseUrl)
	{
		// return if already absolute URL
		if ( parse_url($sRelUrl, PHP_URL_SCHEME) != '' ) {
			return $sRelUrl;
		}

		// queries and anchors
		if ( $sRelUrl[0] == '#' or $sRelUrl[0] == '?' ) {
			return $sBaseUrl . $sRelUrl;
		}

		$aParts = parse_url($sBaseUrl);

		// remove non-directory element from path
		$aParts['path'] = preg_replace('#/[^/]*$#', '', $aParts['path']);

		// destroy path if relative url points to root
		if ( $sRelUrl[0] == '/' ) {
			$aParts['path'] = '';
		}

		// dirty absolute URL
		$sAbs = $aParts['host'];
		if ( isset($aParts['port']) and $aParts['port'] ) {
			$sAbs .= ':' . $aParts['port'];
		}
		$sAbs .= $aParts['path'] . '/' . $sRelUrl;

		// replace '//' or '/./' or '/foo/../' with '/'
		$aRe = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
		for ( $i = 1; $i > 0; $sAbs = preg_replace($aRe, '/', $sAbs, -1, $i) );

		return $aParts['scheme'] . '://' . $sAbs;
	}

	public function __toString()
	{
		return (string) $this->send();
	}
}