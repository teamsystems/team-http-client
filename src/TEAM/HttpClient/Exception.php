<?php

namespace TEAM\HttpClient;

/**
 * HTTP Client Exception Class
 *
 * @package		team-http-client
 * @author		Mike Frank <mike.frank@teamsystems.ca>
 */
class Exception extends \Exception
{

}