<?php

namespace TEAM\HttpClient;

/**
 * Cookie Jar persists and stores cookie information between requests
 *
 * @package		team-http-client
 * @author		Mike Frank <mike.frank@teamsystems.ca>
 */
class CookieJar
{
	/**
	 * Url which made the cookie
	 * @var string
	 */
	protected $_sUrl;

	/**
	 * Decoded cookies
	 *
	 * @var array
	 */
	public $aCookies = array();

	/**
	 *
	 * @param string $sUrl
	 */
	public function __construct($sUrl)
	{
		assert(is_string($sUrl));

		$this->_sUrl = $sUrl;
	}

	/**
	 * Add a raw cookie to the jar
	 *
	 * @param string $sCookie
	 */
	public function add($sCookie)
	{
		assert(is_string($sCookie));

		$aCookie = self::parseCookie($sCookie);

		$this->aCookies[] = $aCookie;
	}

	/**
	 * Retrieve cookies specific to a url
	 *
	 * @param string $sUrl
	 * @return array
	 */
	public function get($sUrl)
	{
		assert(is_string($sUrl));

		$aReturn = array();

		$aUrl = array_merge(array(
			'path' => '/'
		), parse_url($sUrl));

		foreach ($this->aCookies as $aCookie) {

			if ( $aCookie['expires'] !== NULL and time() > $aCookie['expires'] ) {
				continue;
			}

			// TODO Though not used, account for max-age

			if ($aCookie['secure'] === TRUE and $aUrl['scheme'] != 'https') {
				// Intended for secure connections only
				continue;
			}

			if ( $aCookie['domain'] !== NULL ) {
				if ( $aCookie['domain'] != $aUrl['host'] ) {
					if ( ! preg_match('/' . preg_quote($aCookie['domain']) . '$/i', $aUrl['host']) ) {
						// Cookie is intended for another domain
						continue;
					}
				}
			}

			if ( strpos($aUrl['path'], $aCookie['path']) !== 0 ) {
				// Wrong path
				continue;
			}

			$aReturn[] = $aCookie;

		}

		return $aReturn;
	}

	/**
	 * Build the Cookie: header based on a url
	 *
	 * @param string $sUrl
	 * @return string
	 */
	public function buildHeader($sUrl)
	{
		assert(is_string($sUrl));

		$aReturn = array();

		foreach ( $this->get($sUrl) as $aCookie ) {
			$aReturn[] = $aCookie['value']['key'] . '=' . urlencode($aCookie['value']['value']);
		}

		return implode('; ', $aReturn);
	}

	/**
	 * Parse a Set-Cookie header
	 *
	 * @param string $sCookie
	 * @return array
	 */
	public static function parseCookie($sCookie)
	{
		assert(is_string($sCookie));

		$aCookie = array(
			'value' => array(
				'key' => '',
				'value' => NULL
			),
			'domain' => NULL,
			'expires' => NULL,
			'path' => '/',
			'secure' => FALSE,
			'comment' => NULL,
			'max-age' => NULL,
			'version' => NULL,
			'httponly' => FALSE
		);

		$aSplit = explode(';', $sCookie);
		foreach ( $aSplit as $sData ) {
			if ( strpos($sData, '=') ) {
				list($sKey, $sValue) = explode('=', $sData, 2);
				$sKey = trim($sKey);
				$sValue = urldecode(trim($sValue));
			}
			else {
				$sKey = trim($sData);
				$sValue = NULL;
			}

			$sKeyLower = strtolower($sKey);

			if ( $sKeyLower == 'expires' ) {
				$sValue = strtotime($sValue);
			}

			if ( $sKeyLower == 'domain' ) {
				$sValue = ltrim($sValue, '.');
			}

			if ( $sKeyLower == 'secure' or $sKeyLower == 'httponly' ) {
				$sValue = TRUE;
			}

			if ( in_array($sKeyLower, array('domain', 'expires', 'path', 'secure', 'comment', 'max-age', 'version', 'httponly')) ) {
				$aCookie[$sKeyLower] = $sValue;
			}
			else {
				$aCookie['value']['key'] = $sKey;
				$aCookie['value']['value'] = $sValue;
			}
		}

		return $aCookie;
	}
}