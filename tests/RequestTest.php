<?php

use TEAM\HttpClient\Request;

class RequestTest extends PHPUnit_Framework_TestCase
{
    public function testHttpGet()
    {
        $o = Request::get('http://birchwood.ca/')->send();

        $this->assertEquals(200, $o->iStatus);
    }

    public function testRelative2Absolute()
    {
        $this->assertEquals('http://example.com/mike', Request::relative2Absolute('/mike', 'http://example.com/hi'));
        $this->assertEquals('http://example.com/hi/mike', Request::relative2Absolute('mike', 'http://example.com/hi/'));
        $this->assertEquals('http://example.com/hi/mike?g=55', Request::relative2Absolute('mike?g=55', 'http://example.com/hi/'));
    }
}
