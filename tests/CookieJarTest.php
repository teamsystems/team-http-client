<?php

use TEAM\HttpClient\CookieJar;

class CookieJarTest extends PHPUnit_Framework_TestCase
{
    const TEST_COOKIE = 'name=Mike; domain=hurrdurr.com; path=/jellyBean; httponly; secure; expires=56; max-age=5; version=1';

    public function testParseCookie()
    {
        $aCookie = CookieJar::parseCookie(self::TEST_COOKIE);

        $this->assertEquals('Mike', $aCookie['value']['value']);
        $this->assertEquals('hurrdurr.com', $aCookie['domain']);
        $this->assertEquals('/jellyBean', $aCookie['path']);
        $this->assertEquals(TRUE, $aCookie['httponly']);
        $this->assertEquals(TRUE, $aCookie['secure']);
        $this->assertEquals(strtotime('56'), $aCookie['expires']);
        $this->assertEquals('5', $aCookie['max-age']);
        $this->assertEquals('1', $aCookie['version']);
    }
}